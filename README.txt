An interactive recipe builder based on the rules for Compulsive Eaters Anonymous (cea-how).
This app allows users to create custom recipes while following the required portion
amounts and allows you to change how many portions so that recipes can automatically
adjust, cutting out all the math.

The user can add/edit ingredients, set type of food, portion size, and measurement type.