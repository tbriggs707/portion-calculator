﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PortionCalculator.Food_Library
{
    public class XML_IE
    {
        private static XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<FoodItem>));

        public List<FoodItem> ReadFromFile(string filePath)
        {
            StreamReader reader = new StreamReader(filePath);
            List<FoodItem> data = xmlSerializer.Deserialize(reader.BaseStream) as List<FoodItem>;
            reader.Close();

            return data;
        }

        public void WriteResults(string filePath, List<FoodItem> data)
        {
            StreamWriter writer = new StreamWriter(filePath);
            xmlSerializer.Serialize(writer, data);
            writer.Close();
        }
    }
}
