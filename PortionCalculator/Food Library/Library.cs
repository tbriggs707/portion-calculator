﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PortionCalculator.Food_Library
{
    [DataContract]
    public class Library
    {
        [DataMember]
        public List<FoodItem> Items { get; set; }

        private XML_IE ie = new XML_IE();
        private object myLock = new object();

        public void AddFoodItem(FoodItem item)
        {
            if (item != null)
            {
                lock (myLock)
                {
                    Items.Add(item);
                }
            }
        }
        public void RemoveFoodItem(FoodItem item)
        {
            if (item != null)
            {
                lock (myLock)
                {
                    Items.Remove(item);
                }
            }
        }

        public bool Contains(string name)
        {
            foreach (var item in Items)
                if (item.Name == name)
                    return true;

            return false;
        }
        public FoodItem GetItem(string name)
        {
            foreach (FoodItem item in Items)
                if (item.Name + " - " + item.FullAmount == name || item.Name == name)
                    return item;

            return null;
        }

        internal List<string> GetItems(string category)
        {
            List<string> toReturn = new List<string>();

            foreach (var item in Items)
            {
                string itemCategory = item.Category.ToString();
                if (itemCategory == category && !toReturn.Contains(itemCategory))
                    toReturn.Add(item.Name + " - " + item.FullAmount);
            }

            return toReturn;
        }
        
        public Library()
        {
            string filePath = @"Food Library\library.xml";
            Items = (ie.ReadFromFile(filePath)).OrderBy(item => item.Name).ToList(); ;
        }
        public void Save()
        {
            ie.WriteResults(@"Food Library\library.xml", Items);
        }
    }
}
