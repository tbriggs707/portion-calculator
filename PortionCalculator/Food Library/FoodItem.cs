﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortionCalculator.Food_Library
{
    public class FoodItem
    {
        public enum CategoryName { Protein, Fruit, Vegetable, Fat, Condiment, Grain, Dressing }; 
        public enum Metric { oz, c, tsp, Tbsp, whole };

        public string Name { get; set; } // ex: yogurt, egg, chicken
        public double Amount { get; set; } // per serving, ex: 8, 1/2
        public Metric Measurement { get; set; }
        public CategoryName Category { get; set; }

        public string FullAmount
        {
            get
            {
                return GetAmount();
            }
        }
        public bool validFoodItem
        {
            get
            {
                return !string.IsNullOrEmpty(Name) && Amount > 0 && !string.IsNullOrEmpty(Measurement.ToString()) ;
            }
        }
        public string GetAmount()
        {
            string toReturn = "Invalid Amount";

            if (validFoodItem)
                toReturn = Amount.ToString() + Measurement.ToString();

            toReturn = toReturn.Replace("whole", "wh");
                
            return toReturn;
        }
    }
}
