﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortionCalculator.Food_Library;
using System.IO;

namespace PortionCalculator.Recipe
{
    public class Recipe
    {
        private Library library;
        private XML_IE ie = new XML_IE();
        private List<FoodItem.CategoryName> categories;

        public string RecipeName { get; set; }
        public List<FoodItem> Ingredients { get; set; }
        public Dictionary<string, double> Remaining { get; set; }
        public double Servings { get; set; }
        
        public Recipe(Library lib)
        {
            library = lib;
            Ingredients = new List<FoodItem>();
            Servings = 1.0;
            InitializeCategories();
            InitializeRemianing();
            RecipeName = string.Empty;
        }

        private void InitializeCategories()
        {
            categories = new List<FoodItem.CategoryName>()
            {
                FoodItem.CategoryName.Condiment,
                FoodItem.CategoryName.Dressing,
                FoodItem.CategoryName.Fat,
                FoodItem.CategoryName.Fruit,
                FoodItem.CategoryName.Grain,
                FoodItem.CategoryName.Protein,
                FoodItem.CategoryName.Vegetable
            };
        }

        private void InitializeRemianing()
        {
            Remaining = new Dictionary<string, double>();

            foreach (FoodItem.CategoryName category in categories)
                Remaining.Add(category.ToString(), Servings);
        }

        public void UpdateServings(string newServingsAmount)
        {
            double newServings = 0.0;
            bool success = double.TryParse(newServingsAmount, out newServings);
            newServings = Math.Round(newServings, 2);

            if (success && newServings > 0)
            {
                ResetAllRemaining(newServings);

                foreach (FoodItem.CategoryName category in categories)
                {
                    foreach (FoodItem ingredient in Ingredients)
                    {
                        FoodItem item = library.GetItem(ingredient.Name);

                        if (item == null) continue;

                        if (ingredient.Category == category)
                            RemoveFromRemaining(ingredient, item);
                    }
                }
            }
        }

        public void ResetAllRemaining(double newServings = -1)
        {
            if (newServings == -1)
                Servings = 1;
            else
                Servings = newServings;

            foreach (FoodItem.CategoryName category in categories)
                Remaining[category.ToString()] = Servings;
        }

        public void AddToRemaining(FoodItem ingredient, FoodItem item)
        {
            double percentage = Math.Round(ingredient.Amount / item.Amount, 2);
            string category = ingredient.Category.ToString();

            Remaining[category] += percentage;
            Remaining[category] = Math.Round(Remaining[category], 2);

            if (Remaining[category] > Servings)
                Remaining[category] = Servings;
        }

        internal void RemoveFromRemaining(FoodItem ingredient, FoodItem item)
        {
            double percentage = Math.Round(ingredient.Amount / item.Amount, 2);
            string category = ingredient.Category.ToString();

            Remaining[category] -= percentage;
            Remaining[category] = Math.Round(Remaining[category], 2);

            if (Remaining[category] < 0)
                Remaining[category] = 0;
        }

        public void Save()
        {
            string filePath = @"Recipes\" + RecipeName + ".xml";
            
            ie.WriteResults(filePath, Ingredients);
        }

        public FoodItem GetIngredient(string name)
        {
            foreach (FoodItem ingredient in Ingredients)
                if (ingredient.Name + " - " + ingredient.FullAmount == name.TrimStart(' '))
                    return ingredient;

            return null;
        }

        internal bool ContainsCategory(string category)
        {
            foreach (var ingredient in Ingredients)
                if (ingredient.Category.ToString() == category)
                    return true;

            return false;
        }
    }
}
