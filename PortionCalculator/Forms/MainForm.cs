﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PortionCalculator.Food_Library;
using PortionCalculator.Forms;
using PortionCalculator.Recipe;
using System.IO;

namespace PortionCalculator.Forms
{
    public partial class MainForm : Form
    {
        private Library library;

        public MainForm(Library lib)
        {
            library = lib;
            InitializeComponent();
            CenterToScreen();
        }

        private void addFoodType_Click(object sender, EventArgs e)
        {
            AddFoodType();
        }

        private void AddFoodType()
        {

            throw new NotImplementedException();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void addFoodItemsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form addForm = new AddItem(library);
            addForm.Show();
        }

        private void viewAllFoodItemsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form viewForm = new ViewLibrary(library);
            viewForm.Show();
        }

        private void addFoodItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            library.Save();
            MessageBox.Show("All food items have been saved");
        }

        //TODO: write the help message
        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string message = "Welcome to the Portion Calculator, this message hasn't been written yet...";

            MessageBox.Show(message, "Help", MessageBoxButtons.OK);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            library.Save();
            Close();
        }

        private void addRecipeButton_Click(object sender, EventArgs e)
        {
            Form addRecipe = new AddRecipeForm(library);
            addRecipe.Show();
        }

        private void viewRecipesButton_Click(object sender, EventArgs e)
        {
            Form viewRecipeForm = new ViewRecipes(library);
            viewRecipeForm.Show();
        }

        private void addFoodItemButton_Click(object sender, EventArgs e)
        {
            Form addForm = new AddItem(library);
            addForm.Show();
        }

        private void viewFoodItemButton_Click(object sender, EventArgs e)
        {
            Form viewForm = new ViewLibrary(library);
            viewForm.Show();
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }
    }
}
