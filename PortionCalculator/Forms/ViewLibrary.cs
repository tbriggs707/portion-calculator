﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PortionCalculator.Food_Library;
using System.Windows.Forms.VisualStyles;

namespace PortionCalculator.Forms
{
    public partial class ViewLibrary : Form
    {
        #region Variables
        private Library library;
        private List<FoodItem> proteins;
        private List<FoodItem> fruits;
        private List<FoodItem> vegetables;
        private List<FoodItem> fats;
        private List<FoodItem> condiments;
        private List<FoodItem> grains;
        private List<FoodItem> dressings;
        private List<List<FoodItem>> master;

        private Image deleteImage;
        #endregion

        public ViewLibrary(Library lib)
        {
            library = lib;
            deleteImage = Image.FromFile(@"Graphics\delete.png");
            InitializeLists();
            InitializeComponent();
            CenterToScreen();
            LoadMaster();
            InitializePanels();
        }

        private void LoadMaster()
        {
            master = new List<List<FoodItem>>();

            master.Add(proteins);
            master.Add(fruits);
            master.Add(vegetables);
            master.Add(fats);
            master.Add(condiments);
            master.Add(grains);
            master.Add(dressings);
        }

        private void InitializeLists()
        {
            proteins = new List<FoodItem>();
            fruits = new List<FoodItem>();
            vegetables = new List<FoodItem>();
            fats = new List<FoodItem>();
            condiments = new List<FoodItem>();
            grains = new List<FoodItem>();
            dressings = new List<FoodItem>();

            AddToLists();
        }

        /// <summary>
        /// Adds all food items to separate lists
        /// </summary>
        private void AddToLists()
        {
            foreach (FoodItem item in library.Items)
            {
                switch (item.Category)
                {
                    case FoodItem.CategoryName.Protein:
                        proteins.Add(item);
                        break;

                    case FoodItem.CategoryName.Fruit:
                        fruits.Add(item);
                        break;

                    case FoodItem.CategoryName.Vegetable:
                        vegetables.Add(item);
                        break;

                    case FoodItem.CategoryName.Fat:
                        fats.Add(item);
                        break;

                    case FoodItem.CategoryName.Condiment:
                        condiments.Add(item);
                        break;

                    case FoodItem.CategoryName.Grain:
                        grains.Add(item);
                        break;

                    case FoodItem.CategoryName.Dressing:
                        dressings.Add(item);
                        break;

                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Add all food items to the appropriate panels
        /// </summary>
        private void InitializePanels()
        {
            foreach (List<FoodItem> list in  master)
                UpdatePanel(list);
        }

        private void ClearPanel(List<FoodItem> list)
        {
            if (list == proteins)
                proteinPanel.Controls.Clear();
            else if (list == fruits)
                fruitPanel.Controls.Clear();
            else if (list == vegetables)
                vegetablePanel.Controls.Clear();
            else if (list == fats)
                fatOilPanel.Controls.Clear();
            else if (list == condiments)
                condimentPanel.Controls.Clear();
            else if (list == grains)
                grainPanel.Controls.Clear();
            else if (list == dressings)
                dressingPanel.Controls.Clear();
        }

        /// <summary>
        /// Removes a food item from the Library
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RemoveItem(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            FoodItem item = library.GetItem(btn.Name.Replace("BTN", ""));

            if (item == null)
            {
                MessageBox.Show("Error trying to remove that item, it doesn't exist in the Library.");
                return;
            }

            DialogResult result = MessageBox.Show("Are you sure you want to remove " + item.Name + " from the library?",
                "Confirm Delete", MessageBoxButtons.OKCancel);

            if (result == DialogResult.OK)
            {
                library.RemoveFoodItem(item);
                RemoveFromPanel(item);
            }
        }

        /// <summary>
        /// Updates a panel given the corresponding list
        /// </summary>
        /// <param name="list"></param>
        private void UpdatePanel(List<FoodItem> list)
        {
            ClearPanel(list);
            AddLabelButton(list);
        }

        /// <summary>
        /// Adds a label and a button for each item
        /// </summary>
        /// <param name="list"></param>
        private void AddLabelButton(List<FoodItem> list)
        {
            FlowLayoutPanel panel = GetPanel(list);

            foreach (FoodItem item in list)
            {
                Label label = new Label() { Text = item.Name + " - " + item.FullAmount };
                label.AutoSize = false;
                label.Margin = new Padding(0, 0, 0, 0);
                label.Width = 145;
                panel.Controls.Add(label);

                Button button = new Button() { Text = "", Name = item.Name + "BTN" };
                button.Width = 20;
                button.Margin = new Padding(0, 0, 0, 0);
                button.Image = deleteImage;
                button.FlatStyle = FlatStyle.Flat;
                button.FlatAppearance.BorderSize = 0;
                button.Click += new EventHandler(RemoveItem);
                panel.Controls.Add(button);
            }
        }
        
        /// <summary>
        /// Given a list of food items, return the appropriate panel
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private FlowLayoutPanel GetPanel(List<FoodItem> list)
        {
            if (list == proteins)
                return proteinPanel;
            else if (list == fruits)
                return fruitPanel;
            else if (list == vegetables)
                return vegetablePanel;
            else if (list == fats)
                return fatOilPanel;
            else if (list == condiments)
                return condimentPanel;
            else if (list == grains)
                return grainPanel;
            else if (list == dressings)
                return dressingPanel;

            return null;
        }

        /// <summary>
        /// Removes the food item from the appropriate list and then updates the corresponding panel
        /// </summary>
        /// <param name="item"></param>
        private void RemoveFromPanel(FoodItem item)
        {
            switch (item.Category)
            {
                case FoodItem.CategoryName.Protein:
                    proteins.Remove(item);
                    UpdatePanel(proteins);
                    break;

                case FoodItem.CategoryName.Fruit:
                    fruits.Remove(item);
                    UpdatePanel(fruits);
                    break;

                case FoodItem.CategoryName.Vegetable:
                    vegetables.Remove(item);
                    UpdatePanel(vegetables);
                    break;

                case FoodItem.CategoryName.Fat:
                    fats.Remove(item);
                    UpdatePanel(fats);
                    break;

                case FoodItem.CategoryName.Condiment:
                    condiments.Remove(item);
                    UpdatePanel(condiments);
                    break;

                case FoodItem.CategoryName.Grain:
                    grains.Remove(item);
                    UpdatePanel(grains);
                    break;

                case FoodItem.CategoryName.Dressing:
                    dressings.Remove(item);
                    UpdatePanel(dressings);
                    break;

                default:
                    break;
            }
        }

        private void ViewLibrary_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                Close();
                return true;
            }
            return base.ProcessDialogKey(keyData);
        }
    }
}
