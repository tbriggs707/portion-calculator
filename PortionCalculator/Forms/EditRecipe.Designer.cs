﻿namespace PortionCalculator.Forms
{
    partial class EditRecipe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditRecipe));
            this.separator2 = new System.Windows.Forms.Label();
            this.separator1 = new System.Windows.Forms.Label();
            this.separatorOne = new System.Windows.Forms.Label();
            this.ingredientsLabel = new System.Windows.Forms.Label();
            this.quickAddButton = new System.Windows.Forms.Button();
            this.servingsLabel = new System.Windows.Forms.Label();
            this.servingsTextBox = new System.Windows.Forms.TextBox();
            this.removeButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.recipeNameTextBox = new System.Windows.Forms.TextBox();
            this.recipeNameLabel = new System.Windows.Forms.Label();
            this.ingredientsListBox = new System.Windows.Forms.ListBox();
            this.addIngredientButton = new System.Windows.Forms.Button();
            this.amountToAddTextBox = new System.Windows.Forms.TextBox();
            this.amountToAddLabel = new System.Windows.Forms.Label();
            this.upToAmount = new System.Windows.Forms.Label();
            this.upToLabel = new System.Windows.Forms.Label();
            this.amtRemainingText = new System.Windows.Forms.Label();
            this.amtRemainingLabel = new System.Windows.Forms.Label();
            this.foodItemsListBox = new System.Windows.Forms.ListBox();
            this.categoryListBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // separator2
            // 
            this.separator2.Location = new System.Drawing.Point(21, 261);
            this.separator2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.separator2.Name = "separator2";
            this.separator2.Size = new System.Drawing.Size(542, 2);
            this.separator2.TabIndex = 45;
            // 
            // separator1
            // 
            this.separator1.Location = new System.Drawing.Point(23, 68);
            this.separator1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.separator1.Name = "separator1";
            this.separator1.Size = new System.Drawing.Size(542, 2);
            this.separator1.TabIndex = 44;
            // 
            // separatorOne
            // 
            this.separatorOne.AutoSize = true;
            this.separatorOne.Location = new System.Drawing.Point(45, 56);
            this.separatorOne.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.separatorOne.Name = "separatorOne";
            this.separatorOne.Size = new System.Drawing.Size(0, 13);
            this.separatorOne.TabIndex = 43;
            // 
            // ingredientsLabel
            // 
            this.ingredientsLabel.AutoSize = true;
            this.ingredientsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ingredientsLabel.Location = new System.Drawing.Point(20, 276);
            this.ingredientsLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ingredientsLabel.Name = "ingredientsLabel";
            this.ingredientsLabel.Size = new System.Drawing.Size(93, 20);
            this.ingredientsLabel.TabIndex = 42;
            this.ingredientsLabel.Text = "Ingredients:";
            // 
            // quickAddButton
            // 
            this.quickAddButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quickAddButton.Location = new System.Drawing.Point(136, 541);
            this.quickAddButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.quickAddButton.Name = "quickAddButton";
            this.quickAddButton.Size = new System.Drawing.Size(104, 37);
            this.quickAddButton.TabIndex = 41;
            this.quickAddButton.Text = "Quick Add";
            this.quickAddButton.UseVisualStyleBackColor = true;
            this.quickAddButton.Click += new System.EventHandler(this.quickAddButton_Click);
            // 
            // servingsLabel
            // 
            this.servingsLabel.AutoSize = true;
            this.servingsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.servingsLabel.Location = new System.Drawing.Point(464, 17);
            this.servingsLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.servingsLabel.Name = "servingsLabel";
            this.servingsLabel.Size = new System.Drawing.Size(74, 20);
            this.servingsLabel.TabIndex = 40;
            this.servingsLabel.Text = "Servings:";
            // 
            // servingsTextBox
            // 
            this.servingsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.servingsTextBox.Location = new System.Drawing.Point(539, 15);
            this.servingsTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.servingsTextBox.Name = "servingsTextBox";
            this.servingsTextBox.Size = new System.Drawing.Size(27, 26);
            this.servingsTextBox.TabIndex = 39;
            this.servingsTextBox.TextChanged += new System.EventHandler(this.servingsTextBox_TextChanged);
            this.servingsTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.servingsTextBox_KeyDown);
            // 
            // removeButton
            // 
            this.removeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeButton.Location = new System.Drawing.Point(23, 541);
            this.removeButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(98, 37);
            this.removeButton.TabIndex = 38;
            this.removeButton.Text = "Remove";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // clearButton
            // 
            this.clearButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearButton.Location = new System.Drawing.Point(400, 541);
            this.clearButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(74, 37);
            this.clearButton.TabIndex = 37;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.Location = new System.Drawing.Point(491, 541);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(74, 37);
            this.cancelButton.TabIndex = 36;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButton.Location = new System.Drawing.Point(311, 541);
            this.saveButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(74, 37);
            this.saveButton.TabIndex = 35;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // recipeNameTextBox
            // 
            this.recipeNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recipeNameTextBox.Location = new System.Drawing.Point(125, 15);
            this.recipeNameTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.recipeNameTextBox.Name = "recipeNameTextBox";
            this.recipeNameTextBox.Size = new System.Drawing.Size(222, 26);
            this.recipeNameTextBox.TabIndex = 34;
            this.recipeNameTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.recipeNameTextBox_KeyDown);
            // 
            // recipeNameLabel
            // 
            this.recipeNameLabel.AutoSize = true;
            this.recipeNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recipeNameLabel.Location = new System.Drawing.Point(20, 17);
            this.recipeNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.recipeNameLabel.Name = "recipeNameLabel";
            this.recipeNameLabel.Size = new System.Drawing.Size(109, 20);
            this.recipeNameLabel.TabIndex = 33;
            this.recipeNameLabel.Text = "Recipe Name:";
            // 
            // ingredientsListBox
            // 
            this.ingredientsListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ingredientsListBox.FormattingEnabled = true;
            this.ingredientsListBox.ItemHeight = 20;
            this.ingredientsListBox.Location = new System.Drawing.Point(23, 299);
            this.ingredientsListBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ingredientsListBox.Name = "ingredientsListBox";
            this.ingredientsListBox.Size = new System.Drawing.Size(543, 224);
            this.ingredientsListBox.TabIndex = 32;
            this.ingredientsListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ingredientsListBox_KeyDown);
            // 
            // addIngredientButton
            // 
            this.addIngredientButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addIngredientButton.Location = new System.Drawing.Point(364, 201);
            this.addIngredientButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.addIngredientButton.Name = "addIngredientButton";
            this.addIngredientButton.Size = new System.Drawing.Size(202, 37);
            this.addIngredientButton.TabIndex = 31;
            this.addIngredientButton.Text = "Add Ingredient";
            this.addIngredientButton.UseVisualStyleBackColor = true;
            this.addIngredientButton.Visible = false;
            this.addIngredientButton.Click += new System.EventHandler(this.addIngredientButton_Click);
            // 
            // amountToAddTextBox
            // 
            this.amountToAddTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.amountToAddTextBox.Location = new System.Drawing.Point(490, 159);
            this.amountToAddTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.amountToAddTextBox.Name = "amountToAddTextBox";
            this.amountToAddTextBox.Size = new System.Drawing.Size(76, 26);
            this.amountToAddTextBox.TabIndex = 30;
            this.amountToAddTextBox.TextChanged += new System.EventHandler(this.amountToAddTextBox_TextChanged);
            this.amountToAddTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.amountToAddTextBox_KeyDown);
            // 
            // amountToAddLabel
            // 
            this.amountToAddLabel.AutoSize = true;
            this.amountToAddLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.amountToAddLabel.Location = new System.Drawing.Point(360, 162);
            this.amountToAddLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.amountToAddLabel.Name = "amountToAddLabel";
            this.amountToAddLabel.Size = new System.Drawing.Size(120, 20);
            this.amountToAddLabel.TabIndex = 29;
            this.amountToAddLabel.Text = "Amount to Add:";
            // 
            // upToAmount
            // 
            this.upToAmount.AutoSize = true;
            this.upToAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upToAmount.Location = new System.Drawing.Point(412, 126);
            this.upToAmount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.upToAmount.Name = "upToAmount";
            this.upToAmount.Size = new System.Drawing.Size(29, 20);
            this.upToAmount.TabIndex = 28;
            this.upToAmount.Text = "     ";
            // 
            // upToLabel
            // 
            this.upToLabel.AutoSize = true;
            this.upToLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upToLabel.Location = new System.Drawing.Point(360, 126);
            this.upToLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.upToLabel.Name = "upToLabel";
            this.upToLabel.Size = new System.Drawing.Size(52, 20);
            this.upToLabel.TabIndex = 27;
            this.upToLabel.Text = "Up to:";
            // 
            // amtRemainingText
            // 
            this.amtRemainingText.AutoSize = true;
            this.amtRemainingText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.amtRemainingText.Location = new System.Drawing.Point(523, 93);
            this.amtRemainingText.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.amtRemainingText.Name = "amtRemainingText";
            this.amtRemainingText.Size = new System.Drawing.Size(21, 20);
            this.amtRemainingText.TabIndex = 26;
            this.amtRemainingText.Text = "   ";
            // 
            // amtRemainingLabel
            // 
            this.amtRemainingLabel.AutoSize = true;
            this.amtRemainingLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.amtRemainingLabel.Location = new System.Drawing.Point(360, 93);
            this.amtRemainingLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.amtRemainingLabel.Name = "amtRemainingLabel";
            this.amtRemainingLabel.Size = new System.Drawing.Size(153, 20);
            this.amtRemainingLabel.TabIndex = 25;
            this.amtRemainingLabel.Text = "Amount Remaining: ";
            // 
            // foodItemsListBox
            // 
            this.foodItemsListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.foodItemsListBox.FormattingEnabled = true;
            this.foodItemsListBox.ItemHeight = 20;
            this.foodItemsListBox.Location = new System.Drawing.Point(194, 93);
            this.foodItemsListBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.foodItemsListBox.Name = "foodItemsListBox";
            this.foodItemsListBox.Size = new System.Drawing.Size(153, 144);
            this.foodItemsListBox.TabIndex = 24;
            this.foodItemsListBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.foodItemsListBox_MouseClick);
            this.foodItemsListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.foodItemsListBox_KeyDown);
            // 
            // categoryListBox
            // 
            this.categoryListBox.BackColor = System.Drawing.SystemColors.Window;
            this.categoryListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.categoryListBox.FormattingEnabled = true;
            this.categoryListBox.ItemHeight = 20;
            this.categoryListBox.Items.AddRange(new object[] {
            "Proteins",
            "Fruits",
            "Vegetables",
            "Fats",
            "Condiments",
            "Grains",
            "Dressings"});
            this.categoryListBox.Location = new System.Drawing.Point(23, 93);
            this.categoryListBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.categoryListBox.Name = "categoryListBox";
            this.categoryListBox.Size = new System.Drawing.Size(153, 144);
            this.categoryListBox.TabIndex = 23;
            this.categoryListBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.categoryListBox_MouseClick);
            this.categoryListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.categoryListBox_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(233, 271);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 25);
            this.label1.TabIndex = 46;
            this.label1.Text = "EDIT MODE";
            // 
            // EditRecipe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 593);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.separator2);
            this.Controls.Add(this.separator1);
            this.Controls.Add(this.separatorOne);
            this.Controls.Add(this.ingredientsLabel);
            this.Controls.Add(this.quickAddButton);
            this.Controls.Add(this.servingsLabel);
            this.Controls.Add(this.servingsTextBox);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.recipeNameTextBox);
            this.Controls.Add(this.recipeNameLabel);
            this.Controls.Add(this.ingredientsListBox);
            this.Controls.Add(this.addIngredientButton);
            this.Controls.Add(this.amountToAddTextBox);
            this.Controls.Add(this.amountToAddLabel);
            this.Controls.Add(this.upToAmount);
            this.Controls.Add(this.upToLabel);
            this.Controls.Add(this.amtRemainingText);
            this.Controls.Add(this.amtRemainingLabel);
            this.Controls.Add(this.foodItemsListBox);
            this.Controls.Add(this.categoryListBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "EditRecipe";
            this.Text = "Edit Recipe";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EditRecipe_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label separator2;
        private System.Windows.Forms.Label separator1;
        private System.Windows.Forms.Label separatorOne;
        private System.Windows.Forms.Label ingredientsLabel;
        private System.Windows.Forms.Button quickAddButton;
        private System.Windows.Forms.Label servingsLabel;
        private System.Windows.Forms.TextBox servingsTextBox;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.TextBox recipeNameTextBox;
        private System.Windows.Forms.Label recipeNameLabel;
        private System.Windows.Forms.ListBox ingredientsListBox;
        private System.Windows.Forms.Button addIngredientButton;
        private System.Windows.Forms.TextBox amountToAddTextBox;
        private System.Windows.Forms.Label amountToAddLabel;
        private System.Windows.Forms.Label upToAmount;
        private System.Windows.Forms.Label upToLabel;
        private System.Windows.Forms.Label amtRemainingText;
        private System.Windows.Forms.Label amtRemainingLabel;
        private System.Windows.Forms.ListBox foodItemsListBox;
        private System.Windows.Forms.ListBox categoryListBox;
        private System.Windows.Forms.Label label1;
    }
}