﻿namespace PortionCalculator.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addFoodItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addRecipeButton = new System.Windows.Forms.Button();
            this.viewRecipesButton = new System.Windows.Forms.Button();
            this.viewFoodItemButton = new System.Windows.Forms.Button();
            this.addFoodItemButton = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(476, 28);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addFoodItemToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // addFoodItemToolStripMenuItem
            // 
            this.addFoodItemToolStripMenuItem.Name = "addFoodItemToolStripMenuItem";
            this.addFoodItemToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.addFoodItemToolStripMenuItem.Size = new System.Drawing.Size(165, 26);
            this.addFoodItemToolStripMenuItem.Text = "Save";
            this.addFoodItemToolStripMenuItem.Click += new System.EventHandler(this.addFoodItemToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(165, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(62, 24);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(140, 26);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // addRecipeButton
            // 
            this.addRecipeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addRecipeButton.Image = ((System.Drawing.Image)(resources.GetObject("addRecipeButton.Image")));
            this.addRecipeButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.addRecipeButton.Location = new System.Drawing.Point(30, 43);
            this.addRecipeButton.Name = "addRecipeButton";
            this.addRecipeButton.Size = new System.Drawing.Size(186, 121);
            this.addRecipeButton.TabIndex = 3;
            this.addRecipeButton.Text = "Add Recipe";
            this.addRecipeButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.addRecipeButton.UseVisualStyleBackColor = true;
            this.addRecipeButton.Click += new System.EventHandler(this.addRecipeButton_Click);
            // 
            // viewRecipesButton
            // 
            this.viewRecipesButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewRecipesButton.Image = ((System.Drawing.Image)(resources.GetObject("viewRecipesButton.Image")));
            this.viewRecipesButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.viewRecipesButton.Location = new System.Drawing.Point(30, 185);
            this.viewRecipesButton.Name = "viewRecipesButton";
            this.viewRecipesButton.Size = new System.Drawing.Size(186, 122);
            this.viewRecipesButton.TabIndex = 4;
            this.viewRecipesButton.Text = "View Recipes";
            this.viewRecipesButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.viewRecipesButton.UseVisualStyleBackColor = true;
            this.viewRecipesButton.Click += new System.EventHandler(this.viewRecipesButton_Click);
            // 
            // viewFoodItemButton
            // 
            this.viewFoodItemButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewFoodItemButton.Image = ((System.Drawing.Image)(resources.GetObject("viewFoodItemButton.Image")));
            this.viewFoodItemButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.viewFoodItemButton.Location = new System.Drawing.Point(253, 185);
            this.viewFoodItemButton.Name = "viewFoodItemButton";
            this.viewFoodItemButton.Size = new System.Drawing.Size(186, 122);
            this.viewFoodItemButton.TabIndex = 6;
            this.viewFoodItemButton.Text = "View Food Items";
            this.viewFoodItemButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.viewFoodItemButton.UseVisualStyleBackColor = true;
            this.viewFoodItemButton.Click += new System.EventHandler(this.viewFoodItemButton_Click);
            // 
            // addFoodItemButton
            // 
            this.addFoodItemButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addFoodItemButton.Image = ((System.Drawing.Image)(resources.GetObject("addFoodItemButton.Image")));
            this.addFoodItemButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.addFoodItemButton.Location = new System.Drawing.Point(253, 43);
            this.addFoodItemButton.Name = "addFoodItemButton";
            this.addFoodItemButton.Size = new System.Drawing.Size(186, 121);
            this.addFoodItemButton.TabIndex = 5;
            this.addFoodItemButton.Text = "Add Food Item";
            this.addFoodItemButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.addFoodItemButton.UseVisualStyleBackColor = true;
            this.addFoodItemButton.Click += new System.EventHandler(this.addFoodItemButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 339);
            this.Controls.Add(this.viewFoodItemButton);
            this.Controls.Add(this.addFoodItemButton);
            this.Controls.Add(this.viewRecipesButton);
            this.Controls.Add(this.addRecipeButton);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.Text = "Portion Calculator";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addFoodItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Button addRecipeButton;
        private System.Windows.Forms.Button viewRecipesButton;
        private System.Windows.Forms.Button viewFoodItemButton;
        private System.Windows.Forms.Button addFoodItemButton;
    }
}

