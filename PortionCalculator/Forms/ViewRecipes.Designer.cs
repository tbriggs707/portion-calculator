﻿namespace PortionCalculator.Forms
{
    partial class ViewRecipes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewRecipes));
            this.recipeNamesListBox = new System.Windows.Forms.ListBox();
            this.recipesLabel = new System.Windows.Forms.Label();
            this.ingredientsListBox = new System.Windows.Forms.ListBox();
            this.ingredientsLabel = new System.Windows.Forms.Label();
            this.editRecipeButton = new System.Windows.Forms.Button();
            this.deleteRecipeButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // recipeNamesListBox
            // 
            this.recipeNamesListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recipeNamesListBox.FormattingEnabled = true;
            this.recipeNamesListBox.ItemHeight = 25;
            this.recipeNamesListBox.Location = new System.Drawing.Point(30, 56);
            this.recipeNamesListBox.Name = "recipeNamesListBox";
            this.recipeNamesListBox.Size = new System.Drawing.Size(307, 604);
            this.recipeNamesListBox.TabIndex = 0;
            this.recipeNamesListBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.recipeNamesListBox_MouseClick);
            this.recipeNamesListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.recipeNamesListBox_KeyDown);
            // 
            // recipesLabel
            // 
            this.recipesLabel.AutoSize = true;
            this.recipesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recipesLabel.Location = new System.Drawing.Point(25, 19);
            this.recipesLabel.Name = "recipesLabel";
            this.recipesLabel.Size = new System.Drawing.Size(88, 25);
            this.recipesLabel.TabIndex = 1;
            this.recipesLabel.Text = "Recipes:";
            // 
            // ingredientsListBox
            // 
            this.ingredientsListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ingredientsListBox.FormattingEnabled = true;
            this.ingredientsListBox.ItemHeight = 25;
            this.ingredientsListBox.Location = new System.Drawing.Point(382, 56);
            this.ingredientsListBox.Name = "ingredientsListBox";
            this.ingredientsListBox.Size = new System.Drawing.Size(307, 604);
            this.ingredientsListBox.TabIndex = 2;
            this.ingredientsListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ingredientsListBox_KeyDown);
            // 
            // ingredientsLabel
            // 
            this.ingredientsLabel.AutoSize = true;
            this.ingredientsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ingredientsLabel.Location = new System.Drawing.Point(377, 19);
            this.ingredientsLabel.Name = "ingredientsLabel";
            this.ingredientsLabel.Size = new System.Drawing.Size(114, 25);
            this.ingredientsLabel.TabIndex = 3;
            this.ingredientsLabel.Text = "Ingredients:";
            // 
            // editRecipeButton
            // 
            this.editRecipeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editRecipeButton.Location = new System.Drawing.Point(30, 676);
            this.editRecipeButton.Name = "editRecipeButton";
            this.editRecipeButton.Size = new System.Drawing.Size(162, 34);
            this.editRecipeButton.TabIndex = 4;
            this.editRecipeButton.Text = "Edit Recipe";
            this.editRecipeButton.UseVisualStyleBackColor = true;
            this.editRecipeButton.Click += new System.EventHandler(this.editRecipeButton_Click);
            // 
            // deleteRecipeButton
            // 
            this.deleteRecipeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteRecipeButton.Location = new System.Drawing.Point(236, 676);
            this.deleteRecipeButton.Name = "deleteRecipeButton";
            this.deleteRecipeButton.Size = new System.Drawing.Size(162, 34);
            this.deleteRecipeButton.TabIndex = 5;
            this.deleteRecipeButton.Text = "Delete Recipe";
            this.deleteRecipeButton.UseVisualStyleBackColor = true;
            this.deleteRecipeButton.Click += new System.EventHandler(this.deleteRecipeButton_Click);
            // 
            // ViewRecipes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(730, 722);
            this.Controls.Add(this.deleteRecipeButton);
            this.Controls.Add(this.editRecipeButton);
            this.Controls.Add(this.ingredientsLabel);
            this.Controls.Add(this.ingredientsListBox);
            this.Controls.Add(this.recipesLabel);
            this.Controls.Add(this.recipeNamesListBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ViewRecipes";
            this.Text = "View Recipes";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ViewRecipes_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox recipeNamesListBox;
        private System.Windows.Forms.Label recipesLabel;
        private System.Windows.Forms.ListBox ingredientsListBox;
        private System.Windows.Forms.Label ingredientsLabel;
        private System.Windows.Forms.Button editRecipeButton;
        private System.Windows.Forms.Button deleteRecipeButton;
    }
}