﻿namespace PortionCalculator.Forms
{
    partial class ViewLibrary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewLibrary));
            this.proteinLabel = new System.Windows.Forms.Label();
            this.grainLabel = new System.Windows.Forms.Label();
            this.condimentLabel = new System.Windows.Forms.Label();
            this.fatLabel = new System.Windows.Forms.Label();
            this.vegLabel = new System.Windows.Forms.Label();
            this.fruitLabel = new System.Windows.Forms.Label();
            this.dressingLabel = new System.Windows.Forms.Label();
            this.proteinPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.fruitPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.vegetablePanel = new System.Windows.Forms.FlowLayoutPanel();
            this.fatOilPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.condimentPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.grainPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.dressingPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.viewLibraryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.viewLibraryBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // proteinLabel
            // 
            this.proteinLabel.AutoSize = true;
            this.proteinLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.proteinLabel.Location = new System.Drawing.Point(16, 16);
            this.proteinLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.proteinLabel.Name = "proteinLabel";
            this.proteinLabel.Size = new System.Drawing.Size(110, 29);
            this.proteinLabel.TabIndex = 0;
            this.proteinLabel.Text = "Proteins";
            // 
            // grainLabel
            // 
            this.grainLabel.AutoSize = true;
            this.grainLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grainLabel.Location = new System.Drawing.Point(1107, 16);
            this.grainLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.grainLabel.Name = "grainLabel";
            this.grainLabel.Size = new System.Drawing.Size(89, 29);
            this.grainLabel.TabIndex = 1;
            this.grainLabel.Text = "Grains";
            // 
            // condimentLabel
            // 
            this.condimentLabel.AutoSize = true;
            this.condimentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.condimentLabel.Location = new System.Drawing.Point(1379, 324);
            this.condimentLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.condimentLabel.Name = "condimentLabel";
            this.condimentLabel.Size = new System.Drawing.Size(152, 29);
            this.condimentLabel.TabIndex = 2;
            this.condimentLabel.Text = "Condiments";
            // 
            // fatLabel
            // 
            this.fatLabel.AutoSize = true;
            this.fatLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fatLabel.Location = new System.Drawing.Point(836, 16);
            this.fatLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.fatLabel.Name = "fatLabel";
            this.fatLabel.Size = new System.Drawing.Size(141, 29);
            this.fatLabel.TabIndex = 3;
            this.fatLabel.Text = "Fats && Oils";
            // 
            // vegLabel
            // 
            this.vegLabel.AutoSize = true;
            this.vegLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vegLabel.Location = new System.Drawing.Point(561, 16);
            this.vegLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.vegLabel.Name = "vegLabel";
            this.vegLabel.Size = new System.Drawing.Size(145, 29);
            this.vegLabel.TabIndex = 4;
            this.vegLabel.Text = "Vegetables";
            // 
            // fruitLabel
            // 
            this.fruitLabel.AutoSize = true;
            this.fruitLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fruitLabel.Location = new System.Drawing.Point(287, 16);
            this.fruitLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.fruitLabel.Name = "fruitLabel";
            this.fruitLabel.Size = new System.Drawing.Size(79, 29);
            this.fruitLabel.TabIndex = 5;
            this.fruitLabel.Text = "Fruits";
            // 
            // dressingLabel
            // 
            this.dressingLabel.AutoSize = true;
            this.dressingLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dressingLabel.Location = new System.Drawing.Point(1379, 16);
            this.dressingLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.dressingLabel.Name = "dressingLabel";
            this.dressingLabel.Size = new System.Drawing.Size(130, 29);
            this.dressingLabel.TabIndex = 6;
            this.dressingLabel.Text = "Dressings";
            // 
            // proteinPanel
            // 
            this.proteinPanel.AutoScroll = true;
            this.proteinPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.proteinPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.proteinPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.proteinPanel.Location = new System.Drawing.Point(21, 49);
            this.proteinPanel.Margin = new System.Windows.Forms.Padding(4);
            this.proteinPanel.Name = "proteinPanel";
            this.proteinPanel.Size = new System.Drawing.Size(226, 637);
            this.proteinPanel.TabIndex = 7;
            // 
            // fruitPanel
            // 
            this.fruitPanel.AutoScroll = true;
            this.fruitPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.fruitPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fruitPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fruitPanel.Location = new System.Drawing.Point(292, 49);
            this.fruitPanel.Margin = new System.Windows.Forms.Padding(4);
            this.fruitPanel.Name = "fruitPanel";
            this.fruitPanel.Size = new System.Drawing.Size(226, 637);
            this.fruitPanel.TabIndex = 8;
            // 
            // vegetablePanel
            // 
            this.vegetablePanel.AutoScroll = true;
            this.vegetablePanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.vegetablePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.vegetablePanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vegetablePanel.Location = new System.Drawing.Point(566, 49);
            this.vegetablePanel.Margin = new System.Windows.Forms.Padding(4);
            this.vegetablePanel.Name = "vegetablePanel";
            this.vegetablePanel.Size = new System.Drawing.Size(226, 637);
            this.vegetablePanel.TabIndex = 9;
            // 
            // fatOilPanel
            // 
            this.fatOilPanel.AutoScroll = true;
            this.fatOilPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.fatOilPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fatOilPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fatOilPanel.Location = new System.Drawing.Point(841, 49);
            this.fatOilPanel.Margin = new System.Windows.Forms.Padding(4);
            this.fatOilPanel.Name = "fatOilPanel";
            this.fatOilPanel.Size = new System.Drawing.Size(226, 637);
            this.fatOilPanel.TabIndex = 10;
            // 
            // condimentPanel
            // 
            this.condimentPanel.AutoScroll = true;
            this.condimentPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.condimentPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.condimentPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.condimentPanel.Location = new System.Drawing.Point(1384, 357);
            this.condimentPanel.Margin = new System.Windows.Forms.Padding(4);
            this.condimentPanel.Name = "condimentPanel";
            this.condimentPanel.Size = new System.Drawing.Size(226, 329);
            this.condimentPanel.TabIndex = 11;
            // 
            // grainPanel
            // 
            this.grainPanel.AutoScroll = true;
            this.grainPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.grainPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.grainPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grainPanel.Location = new System.Drawing.Point(1112, 49);
            this.grainPanel.Margin = new System.Windows.Forms.Padding(4);
            this.grainPanel.Name = "grainPanel";
            this.grainPanel.Size = new System.Drawing.Size(226, 637);
            this.grainPanel.TabIndex = 12;
            // 
            // dressingPanel
            // 
            this.dressingPanel.AutoScroll = true;
            this.dressingPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.dressingPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dressingPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dressingPanel.Location = new System.Drawing.Point(1384, 49);
            this.dressingPanel.Margin = new System.Windows.Forms.Padding(4);
            this.dressingPanel.Name = "dressingPanel";
            this.dressingPanel.Size = new System.Drawing.Size(226, 229);
            this.dressingPanel.TabIndex = 13;
            // 
            // viewLibraryBindingSource
            // 
            this.viewLibraryBindingSource.DataSource = typeof(PortionCalculator.Forms.ViewLibrary);
            // 
            // ViewLibrary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1637, 715);
            this.Controls.Add(this.fatOilPanel);
            this.Controls.Add(this.dressingPanel);
            this.Controls.Add(this.grainPanel);
            this.Controls.Add(this.condimentPanel);
            this.Controls.Add(this.vegetablePanel);
            this.Controls.Add(this.fruitPanel);
            this.Controls.Add(this.proteinPanel);
            this.Controls.Add(this.dressingLabel);
            this.Controls.Add(this.fruitLabel);
            this.Controls.Add(this.vegLabel);
            this.Controls.Add(this.fatLabel);
            this.Controls.Add(this.condimentLabel);
            this.Controls.Add(this.grainLabel);
            this.Controls.Add(this.proteinLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ViewLibrary";
            this.Text = "View All Food Items";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ViewLibrary_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.viewLibraryBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label proteinLabel;
        private System.Windows.Forms.Label grainLabel;
        private System.Windows.Forms.Label condimentLabel;
        private System.Windows.Forms.Label fatLabel;
        private System.Windows.Forms.Label vegLabel;
        private System.Windows.Forms.Label fruitLabel;
        private System.Windows.Forms.Label dressingLabel;
        private System.Windows.Forms.FlowLayoutPanel proteinPanel;
        private System.Windows.Forms.BindingSource viewLibraryBindingSource;
        private System.Windows.Forms.FlowLayoutPanel fruitPanel;
        private System.Windows.Forms.FlowLayoutPanel vegetablePanel;
        private System.Windows.Forms.FlowLayoutPanel fatOilPanel;
        private System.Windows.Forms.FlowLayoutPanel condimentPanel;
        private System.Windows.Forms.FlowLayoutPanel grainPanel;
        private System.Windows.Forms.FlowLayoutPanel dressingPanel;
    }
}