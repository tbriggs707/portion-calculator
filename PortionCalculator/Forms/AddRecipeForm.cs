﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PortionCalculator.Food_Library;
using PortionCalculator.Recipe;

namespace PortionCalculator.Forms
{
    public partial class AddRecipeForm : Form
    {
        #region Variables
        private Library library;
        private Recipe.Recipe recipe;
        private List<string> categories;

        #endregion

        public AddRecipeForm(Library master)
        {
            library = master;
            recipe = new Recipe.Recipe(master);

            InitializeComponent();
            CenterToScreen();
            InitializeCategories();
            InitializeSeparators();
        }

        #region List Boxes

        private void categoryListBox_MouseClick(object sender, MouseEventArgs e)
        {
            string category = categoryListBox.SelectedItem.ToString().TrimEnd('s');

            UpdateFoodItems(category);
            UpdateAmountRemaining(category);
        }

        internal void foodItemsListBox_MouseClick(object sender, MouseEventArgs e)
        {
            RefreshFoodItemsListBox();
            amountToAddTextBox.Clear();
            amountToAddTextBox.Focus();
        }

        #endregion

        #region Buttons
        private void addIngredientButton_Click(object sender, EventArgs e)
        {
            string itemName = foodItemsListBox.SelectedItem.ToString();
            FoodItem item = library.GetItem(itemName);
            double result = 0.0;
            bool success = double.TryParse(amountToAddTextBox.Text, out result);

            if (!success)
            {
                MessageBox.Show("Failed to parse ingredient amount, only digits are allowed", "Failure");
                return;
            }

            FoodItem ingredient = new FoodItem()
            {
                Amount = Math.Round(result, 2),
                Category = item.Category,
                Measurement = item.Measurement,
                Name = item.Name
            };

            recipe.Ingredients.Add(ingredient);
            recipe.RemoveFromRemaining(ingredient, item);
            UpdateAmountRemaining(ingredient.Category.ToString());
            UpdateIngredientsList();
        }
        private void saveButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(recipeNameTextBox.Text))
            {
                MessageBox.Show("You must enter a Recipe Name", "Uh oh!");
                return;
            }

            recipe.RecipeName = recipeNameTextBox.Text;
            recipe.Save();

            MessageBox.Show("Recipe saved!", "Save Success");
            Close();
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to remove all ingredients and start over?",
                "Confirm Clear", MessageBoxButtons.OKCancel);

            if (result == DialogResult.OK)
            {
                recipe.Ingredients.Clear();
                recipe.ResetAllRemaining();
                UpdateIngredientsList();
                servingsTextBox.Text = string.Empty;

                foreach (string category in categories)
                    UpdateAmountRemaining(category);
            }
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            if (ingredientsListBox.SelectedItem == null) return;

            string ingredientName = ingredientsListBox.SelectedItem.ToString().TrimStart(' ');

            DialogResult result = MessageBox.Show("Are you sure you want to remove " + ingredientName + "?",
                "Confirm Ingredient Removal", MessageBoxButtons.OKCancel);

            if (result == DialogResult.OK)
            {
                FoodItem ingredient = recipe.GetIngredient(ingredientName);
                recipe.AddToRemaining(ingredient, library.GetItem(ingredient.Name));
                recipe.Ingredients.Remove(ingredient);
                UpdateIngredientsList();

                foreach (string category in categories)
                    UpdateAmountRemaining(category);
            }
        }

        private void quickAddButton_Click(object sender, EventArgs e)
        {
            Form addForm = new AddItem(library);
            addForm.Show();

            string category = categoryListBox.SelectedItem.ToString().TrimEnd('s');
            UpdateFoodItems(category);

            RefreshFoodItemsListBox();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
        #endregion

        #region Text Boxes
        private void amountToAddTextBox_TextChanged(object sender, EventArgs e)
        {
            if (foodItemsListBox.SelectedItem == null) return;

            string itemName = foodItemsListBox.SelectedItem.ToString();
            FoodItem item = library.GetItem(itemName);

            if (itemName == string.Empty) return;

            double result = 0.0;
            bool success = double.TryParse(amountToAddTextBox.Text, out result);
            double remaining = recipe.Remaining[item.Category.ToString()];

            result = Math.Round(result, 2);
            remaining = Math.Round(item.Amount * remaining, 2);

            bool valid = result > 0 && result <= remaining;

            if (success && valid)
                addIngredientButton.Visible = true;
            else
                addIngredientButton.Visible = false;
        }

        private void servingsTextBox_TextChanged(object sender, EventArgs e)
        {
            string newServings = string.IsNullOrEmpty(servingsTextBox.Text) ? "1" : servingsTextBox.Text;
            recipe.UpdateServings(newServings);

            string category = categoryListBox.SelectedItem?.ToString().TrimEnd('s');
            if (!string.IsNullOrEmpty(category))
                UpdateAmountRemaining(category);

            RefreshFoodItemsListBox();
        }
        #endregion 

        #region Keyboard Shortcuts
        private void AddRecipeForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }

        private void amountToAddTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }

        private void categoryListBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }

        private void foodItemsListBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }

        private void ingredientsListBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }

        private void recipeNameTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }

        private void servingsTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }

        #endregion

        #region Helper Methods
        public void RefreshFoodItemsListBox()
        {
            if (foodItemsListBox.SelectedItem == null) return;

            string itemName = foodItemsListBox.SelectedItem.ToString();
            FoodItem item = library.GetItem(itemName);

            double remaining = recipe.Remaining[item.Category.ToString()];

            upToAmount.Text = Math.Round(item.Amount * remaining, 2).ToString() + item.Measurement + " of " + item.Name;

            if (item.Measurement == FoodItem.Metric.whole)
            {
                upToAmount.Text = upToAmount.Text.Replace("of ", "").Replace("whole", " whole");

                if (upToAmount.Text.EndsWith("y"))
                {
                    upToAmount.Text += "ies";
                    upToAmount.Text = upToAmount.Text.Replace("yies", "ies");
                }
                else
                    upToAmount.Text += "s";
            }
        }

        private void UpdateAmountRemaining(string category)
        {
            amtRemainingLabel.Text = category + " Remaining: ";
            amtRemainingText.Text = Math.Round((recipe.Remaining[category] / recipe.Servings) * 100, 2).ToString() + "%";
            upToAmount.Text = string.Empty;
            amountToAddTextBox.Text = string.Empty;
        }

        private void UpdateFoodItems(string category)
        {
            foodItemsListBox.Items.Clear();

            foreach (var item in library.GetItems(category))
                foodItemsListBox.Items.Add(item);
        }
        
        private void UpdateIngredientsList()
        {
            ingredientsListBox.Items.Clear();

            foreach (string category in categories)
            {
                if (recipe.ContainsCategory(category))
                {
                    ingredientsListBox.Items.Add(category);

                    foreach(FoodItem ingredient in recipe.Ingredients)
                        if (ingredient.Category.ToString() == category)
                            ingredientsListBox.Items.Add("     " + ingredient.Name + " - " + ingredient.FullAmount);
                }
            }
        }

        private void InitializeCategories()
        {
            categories = new List<string>()
            {
                "Protein",
                "Fruit",
                "Vegetable",
                "Fat",
                "Condiment",
                "Grain",
                "Dressing"
            };
        }

        private void InitializeSeparators()
        {
            separator1.BorderStyle = BorderStyle.Fixed3D;
            separator2.BorderStyle = BorderStyle.Fixed3D;
        }
        #endregion
    }
}
