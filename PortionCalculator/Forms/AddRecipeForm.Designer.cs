﻿namespace PortionCalculator.Forms
{
    partial class AddRecipeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddRecipeForm));
            this.categoryListBox = new System.Windows.Forms.ListBox();
            this.foodItemsListBox = new System.Windows.Forms.ListBox();
            this.amtRemainingLabel = new System.Windows.Forms.Label();
            this.amtRemainingText = new System.Windows.Forms.Label();
            this.upToLabel = new System.Windows.Forms.Label();
            this.upToAmount = new System.Windows.Forms.Label();
            this.amountToAddLabel = new System.Windows.Forms.Label();
            this.amountToAddTextBox = new System.Windows.Forms.TextBox();
            this.addIngredientButton = new System.Windows.Forms.Button();
            this.ingredientsListBox = new System.Windows.Forms.ListBox();
            this.recipeNameLabel = new System.Windows.Forms.Label();
            this.recipeNameTextBox = new System.Windows.Forms.TextBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.removeButton = new System.Windows.Forms.Button();
            this.servingsTextBox = new System.Windows.Forms.TextBox();
            this.servingsLabel = new System.Windows.Forms.Label();
            this.quickAddButton = new System.Windows.Forms.Button();
            this.ingredientsLabel = new System.Windows.Forms.Label();
            this.separatorOne = new System.Windows.Forms.Label();
            this.separator1 = new System.Windows.Forms.Label();
            this.separator2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // categoryListBox
            // 
            this.categoryListBox.BackColor = System.Drawing.SystemColors.Window;
            this.categoryListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.categoryListBox.FormattingEnabled = true;
            this.categoryListBox.ItemHeight = 20;
            this.categoryListBox.Items.AddRange(new object[] {
            "Proteins",
            "Fruits",
            "Vegetables",
            "Fats",
            "Condiments",
            "Grains",
            "Dressings"});
            this.categoryListBox.Location = new System.Drawing.Point(20, 92);
            this.categoryListBox.Margin = new System.Windows.Forms.Padding(2);
            this.categoryListBox.Name = "categoryListBox";
            this.categoryListBox.Size = new System.Drawing.Size(153, 144);
            this.categoryListBox.TabIndex = 0;
            this.categoryListBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.categoryListBox_MouseClick);
            this.categoryListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.categoryListBox_KeyDown);
            // 
            // foodItemsListBox
            // 
            this.foodItemsListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.foodItemsListBox.FormattingEnabled = true;
            this.foodItemsListBox.ItemHeight = 20;
            this.foodItemsListBox.Location = new System.Drawing.Point(190, 92);
            this.foodItemsListBox.Margin = new System.Windows.Forms.Padding(2);
            this.foodItemsListBox.Name = "foodItemsListBox";
            this.foodItemsListBox.Size = new System.Drawing.Size(153, 144);
            this.foodItemsListBox.TabIndex = 1;
            this.foodItemsListBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.foodItemsListBox_MouseClick);
            this.foodItemsListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.foodItemsListBox_KeyDown);
            // 
            // amtRemainingLabel
            // 
            this.amtRemainingLabel.AutoSize = true;
            this.amtRemainingLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.amtRemainingLabel.Location = new System.Drawing.Point(357, 92);
            this.amtRemainingLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.amtRemainingLabel.Name = "amtRemainingLabel";
            this.amtRemainingLabel.Size = new System.Drawing.Size(153, 20);
            this.amtRemainingLabel.TabIndex = 2;
            this.amtRemainingLabel.Text = "Amount Remaining: ";
            // 
            // amtRemainingText
            // 
            this.amtRemainingText.AutoSize = true;
            this.amtRemainingText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.amtRemainingText.Location = new System.Drawing.Point(520, 92);
            this.amtRemainingText.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.amtRemainingText.Name = "amtRemainingText";
            this.amtRemainingText.Size = new System.Drawing.Size(21, 20);
            this.amtRemainingText.TabIndex = 3;
            this.amtRemainingText.Text = "   ";
            // 
            // upToLabel
            // 
            this.upToLabel.AutoSize = true;
            this.upToLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upToLabel.Location = new System.Drawing.Point(357, 125);
            this.upToLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.upToLabel.Name = "upToLabel";
            this.upToLabel.Size = new System.Drawing.Size(52, 20);
            this.upToLabel.TabIndex = 4;
            this.upToLabel.Text = "Up to:";
            // 
            // upToAmount
            // 
            this.upToAmount.AutoSize = true;
            this.upToAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upToAmount.Location = new System.Drawing.Point(410, 125);
            this.upToAmount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.upToAmount.Name = "upToAmount";
            this.upToAmount.Size = new System.Drawing.Size(29, 20);
            this.upToAmount.TabIndex = 5;
            this.upToAmount.Text = "     ";
            // 
            // amountToAddLabel
            // 
            this.amountToAddLabel.AutoSize = true;
            this.amountToAddLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.amountToAddLabel.Location = new System.Drawing.Point(357, 161);
            this.amountToAddLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.amountToAddLabel.Name = "amountToAddLabel";
            this.amountToAddLabel.Size = new System.Drawing.Size(120, 20);
            this.amountToAddLabel.TabIndex = 6;
            this.amountToAddLabel.Text = "Amount to Add:";
            // 
            // amountToAddTextBox
            // 
            this.amountToAddTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.amountToAddTextBox.Location = new System.Drawing.Point(488, 158);
            this.amountToAddTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.amountToAddTextBox.Name = "amountToAddTextBox";
            this.amountToAddTextBox.Size = new System.Drawing.Size(76, 26);
            this.amountToAddTextBox.TabIndex = 7;
            this.amountToAddTextBox.TextChanged += new System.EventHandler(this.amountToAddTextBox_TextChanged);
            this.amountToAddTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.amountToAddTextBox_KeyDown);
            // 
            // addIngredientButton
            // 
            this.addIngredientButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addIngredientButton.Location = new System.Drawing.Point(361, 200);
            this.addIngredientButton.Margin = new System.Windows.Forms.Padding(2);
            this.addIngredientButton.Name = "addIngredientButton";
            this.addIngredientButton.Size = new System.Drawing.Size(202, 37);
            this.addIngredientButton.TabIndex = 8;
            this.addIngredientButton.Text = "Add Ingredient";
            this.addIngredientButton.UseVisualStyleBackColor = true;
            this.addIngredientButton.Visible = false;
            this.addIngredientButton.Click += new System.EventHandler(this.addIngredientButton_Click);
            // 
            // ingredientsListBox
            // 
            this.ingredientsListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ingredientsListBox.FormattingEnabled = true;
            this.ingredientsListBox.ItemHeight = 20;
            this.ingredientsListBox.Location = new System.Drawing.Point(20, 298);
            this.ingredientsListBox.Margin = new System.Windows.Forms.Padding(2);
            this.ingredientsListBox.Name = "ingredientsListBox";
            this.ingredientsListBox.Size = new System.Drawing.Size(543, 224);
            this.ingredientsListBox.TabIndex = 9;
            this.ingredientsListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ingredientsListBox_KeyDown);
            // 
            // recipeNameLabel
            // 
            this.recipeNameLabel.AutoSize = true;
            this.recipeNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recipeNameLabel.Location = new System.Drawing.Point(16, 16);
            this.recipeNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.recipeNameLabel.Name = "recipeNameLabel";
            this.recipeNameLabel.Size = new System.Drawing.Size(109, 20);
            this.recipeNameLabel.TabIndex = 10;
            this.recipeNameLabel.Text = "Recipe Name:";
            // 
            // recipeNameTextBox
            // 
            this.recipeNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recipeNameTextBox.Location = new System.Drawing.Point(122, 14);
            this.recipeNameTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.recipeNameTextBox.Name = "recipeNameTextBox";
            this.recipeNameTextBox.Size = new System.Drawing.Size(222, 26);
            this.recipeNameTextBox.TabIndex = 11;
            this.recipeNameTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.recipeNameTextBox_KeyDown);
            // 
            // saveButton
            // 
            this.saveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButton.Location = new System.Drawing.Point(308, 540);
            this.saveButton.Margin = new System.Windows.Forms.Padding(2);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(74, 37);
            this.saveButton.TabIndex = 12;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.Location = new System.Drawing.Point(488, 540);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(2);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(74, 37);
            this.cancelButton.TabIndex = 13;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // clearButton
            // 
            this.clearButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearButton.Location = new System.Drawing.Point(398, 540);
            this.clearButton.Margin = new System.Windows.Forms.Padding(2);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(74, 37);
            this.clearButton.TabIndex = 14;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // removeButton
            // 
            this.removeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeButton.Location = new System.Drawing.Point(20, 540);
            this.removeButton.Margin = new System.Windows.Forms.Padding(2);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(98, 37);
            this.removeButton.TabIndex = 15;
            this.removeButton.Text = "Remove";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // servingsTextBox
            // 
            this.servingsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.servingsTextBox.Location = new System.Drawing.Point(536, 14);
            this.servingsTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.servingsTextBox.Name = "servingsTextBox";
            this.servingsTextBox.Size = new System.Drawing.Size(27, 26);
            this.servingsTextBox.TabIndex = 16;
            this.servingsTextBox.TextChanged += new System.EventHandler(this.servingsTextBox_TextChanged);
            this.servingsTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.servingsTextBox_KeyDown);
            // 
            // servingsLabel
            // 
            this.servingsLabel.AutoSize = true;
            this.servingsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.servingsLabel.Location = new System.Drawing.Point(460, 16);
            this.servingsLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.servingsLabel.Name = "servingsLabel";
            this.servingsLabel.Size = new System.Drawing.Size(74, 20);
            this.servingsLabel.TabIndex = 17;
            this.servingsLabel.Text = "Servings:";
            // 
            // quickAddButton
            // 
            this.quickAddButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quickAddButton.Location = new System.Drawing.Point(133, 540);
            this.quickAddButton.Margin = new System.Windows.Forms.Padding(2);
            this.quickAddButton.Name = "quickAddButton";
            this.quickAddButton.Size = new System.Drawing.Size(104, 37);
            this.quickAddButton.TabIndex = 18;
            this.quickAddButton.Text = "Quick Add";
            this.quickAddButton.UseVisualStyleBackColor = true;
            this.quickAddButton.Click += new System.EventHandler(this.quickAddButton_Click);
            // 
            // ingredientsLabel
            // 
            this.ingredientsLabel.AutoSize = true;
            this.ingredientsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ingredientsLabel.Location = new System.Drawing.Point(16, 275);
            this.ingredientsLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ingredientsLabel.Name = "ingredientsLabel";
            this.ingredientsLabel.Size = new System.Drawing.Size(93, 20);
            this.ingredientsLabel.TabIndex = 19;
            this.ingredientsLabel.Text = "Ingredients:";
            // 
            // separatorOne
            // 
            this.separatorOne.AutoSize = true;
            this.separatorOne.Location = new System.Drawing.Point(42, 55);
            this.separatorOne.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.separatorOne.Name = "separatorOne";
            this.separatorOne.Size = new System.Drawing.Size(0, 13);
            this.separatorOne.TabIndex = 20;
            // 
            // separator1
            // 
            this.separator1.Location = new System.Drawing.Point(20, 67);
            this.separator1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.separator1.Name = "separator1";
            this.separator1.Size = new System.Drawing.Size(542, 2);
            this.separator1.TabIndex = 21;
            // 
            // separator2
            // 
            this.separator2.Location = new System.Drawing.Point(18, 260);
            this.separator2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.separator2.Name = "separator2";
            this.separator2.Size = new System.Drawing.Size(542, 2);
            this.separator2.TabIndex = 22;
            // 
            // AddRecipeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(586, 593);
            this.Controls.Add(this.separator2);
            this.Controls.Add(this.separator1);
            this.Controls.Add(this.separatorOne);
            this.Controls.Add(this.ingredientsLabel);
            this.Controls.Add(this.quickAddButton);
            this.Controls.Add(this.servingsLabel);
            this.Controls.Add(this.servingsTextBox);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.recipeNameTextBox);
            this.Controls.Add(this.recipeNameLabel);
            this.Controls.Add(this.ingredientsListBox);
            this.Controls.Add(this.addIngredientButton);
            this.Controls.Add(this.amountToAddTextBox);
            this.Controls.Add(this.amountToAddLabel);
            this.Controls.Add(this.upToAmount);
            this.Controls.Add(this.upToLabel);
            this.Controls.Add(this.amtRemainingText);
            this.Controls.Add(this.amtRemainingLabel);
            this.Controls.Add(this.foodItemsListBox);
            this.Controls.Add(this.categoryListBox);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "AddRecipeForm";
            this.Text = "Add Recipe";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AddRecipeForm_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox categoryListBox;
        private System.Windows.Forms.ListBox foodItemsListBox;
        private System.Windows.Forms.Label amtRemainingLabel;
        private System.Windows.Forms.Label amtRemainingText;
        private System.Windows.Forms.Label upToLabel;
        private System.Windows.Forms.Label upToAmount;
        private System.Windows.Forms.Label amountToAddLabel;
        private System.Windows.Forms.TextBox amountToAddTextBox;
        private System.Windows.Forms.Button addIngredientButton;
        private System.Windows.Forms.ListBox ingredientsListBox;
        private System.Windows.Forms.Label recipeNameLabel;
        private System.Windows.Forms.TextBox recipeNameTextBox;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.TextBox servingsTextBox;
        private System.Windows.Forms.Label servingsLabel;
        private System.Windows.Forms.Button quickAddButton;
        private System.Windows.Forms.Label ingredientsLabel;
        private System.Windows.Forms.Label separatorOne;
        private System.Windows.Forms.Label separator1;
        private System.Windows.Forms.Label separator2;
    }
}