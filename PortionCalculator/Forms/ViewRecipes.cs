﻿using PortionCalculator.Food_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PortionCalculator.Forms
{
    public partial class ViewRecipes : Form
    {
        private List<Recipe.Recipe> recipes;
        private List<string> categories;
        private XML_IE ie = new XML_IE();
        private Library library;

        public bool ValidRecipe { get; set; }

        public ViewRecipes(Library master)
        {
            library = master;
            recipes = ReadInRecipes(master);
            InitializeComponent();
            CenterToScreen();
            RefreshRecipeNamesListBox();
            InitializeCategories();
        }

        private void RefreshRecipeNamesListBox()
        {
            recipeNamesListBox.Items.Clear();

            foreach (var recipe in recipes)
                recipeNamesListBox.Items.Add(recipe.RecipeName);
        }

        private void RefreshIngredientsListBox()
        {
            ingredientsListBox.Items.Clear();

            string selectedRecipeName = recipeNamesListBox.SelectedItem.ToString();
            Recipe.Recipe recipe = recipes.First(r => r.RecipeName == selectedRecipeName);

            foreach (string category in categories)
            {
                if (recipe.ContainsCategory(category))
                {
                    ingredientsListBox.Items.Add(category);

                    foreach (FoodItem ingredient in recipe.Ingredients)
                        if (ingredient.Category.ToString() == category)
                            ingredientsListBox.Items.Add("     " + ingredient.Name + " - " + ingredient.FullAmount);
                }
            }
        }

        private void recipeNamesListBox_MouseClick(object sender, MouseEventArgs e)
        {
            RefreshIngredientsListBox();
        }

        private void ViewRecipes_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }

        private void recipeNamesListBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }

        private void ingredientsListBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }

        private void InitializeCategories()
        {
            categories = new List<string>();

            categories.Add("Protein");
            categories.Add("Fruit");
            categories.Add("Vegetable");
            categories.Add("Fat");
            categories.Add("Condiment");
            categories.Add("Grain");
            categories.Add("Dressing");
        }

        private List<Recipe.Recipe> ReadInRecipes(Library library)
        {
            List<Recipe.Recipe> toReturn = new List<Recipe.Recipe>();

            foreach (string file in Directory.EnumerateFiles(@"Recipes\", "*.xml"))
            {
                Recipe.Recipe recipe = new Recipe.Recipe(library)
                {
                    Ingredients = (ie.ReadFromFile(file)).OrderBy(item => item.Name).ToList(),
                    RecipeName = file.Replace(@"Recipes\", "").Replace(".xml", "")
                };

                toReturn.Add(recipe);
            }

            return toReturn;
        }

        private void editRecipeButton_Click(object sender, EventArgs e)
        {
            if (recipeNamesListBox.SelectedItem == null) return;

            string recipeName = recipeNamesListBox.SelectedItem.ToString();
            Recipe.Recipe recipe = GetRecipe(recipeName);
            bool validRecipe = false;
            Form editRecipe = new EditRecipe(library, recipe, out validRecipe);
            if (validRecipe)
                editRecipe.Show();
        }

        private void deleteRecipeButton_Click(object sender, EventArgs e)
        {
            if (recipeNamesListBox.SelectedItem == null) return;

            string recipeName = recipeNamesListBox.SelectedItem.ToString();

            DialogResult result = MessageBox.Show("Are you sure you want to delete " + recipeName + "?",
                "Confirm Clear", MessageBoxButtons.OKCancel);

            if (result == DialogResult.OK)
            {
                Recipe.Recipe toRemove = GetRecipe(recipeName);

                if (toRemove != null)
                {
                    try
                    {
                        recipes.Remove(toRemove);
                        File.Delete(@"Recipes\" + recipeName + ".xml");
                        RefreshRecipeNamesListBox();
                        ingredientsListBox.Items.Clear();
                    } catch { MessageBox.Show("There was an unknown issue when deleting your recipe.", "Error!"); }
                }
            }
        }

        private Recipe.Recipe GetRecipe(string recipeName)
        {
            foreach (Recipe.Recipe recipe in recipes)
                if (recipe.RecipeName == recipeName)
                    return recipe;
             
            return null;
        }
    }
}
