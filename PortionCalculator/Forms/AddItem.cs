﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PortionCalculator.Food_Library;

namespace PortionCalculator.Forms
{
    public partial class AddItem : Form
    {
        private Library library;

        public AddItem(Library lib)
        {
            library = lib;
            InitializeComponent();
            CenterToScreen();
        }

        #region Events
        public void submitButton_Click(object sender, EventArgs e)
        {
            string name = nameTextBox.Text;
            string amount = amountTextBox.Text;
            string measurement = measurementDropDown.Text;
            string category = categoryDropDown.Text;
            bool validated = false;

            double amt = ValidateInput(name, amount, measurement, category, out validated);

            var t = new Timer();
            t.Interval = 3000; // it will Tick in 3 seconds

            if (validated)
            {
                FoodItem item = new FoodItem()
                {
                    Amount = amt,
                    Name = name,
                    Measurement = (FoodItem.Metric) Enum.Parse(typeof(FoodItem.Metric), measurement, true),
                    Category = (FoodItem.CategoryName) Enum.Parse(typeof(FoodItem.CategoryName), category, true)
                };

                library.AddFoodItem(item);
                ClearFields();
            }
            else
                successLabel.Text = "Failure...";

            successLabel.Visible = true;
            t.Tick += (s, ee) =>
            {
                successLabel.Visible = false; //.Hide();
                t.Stop();
            };
            t.Start();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
        #endregion

        #region Logic
        private void ClearFields()
        {
            nameTextBox.Text = "";
            amountTextBox.Text = "";
            measurementDropDown.Text = "oz";
            categoryDropDown.Text = "Protein";
        }

        /// <summary>
        /// Template method that drives all validation methods
        /// </summary>
        /// <param name="name"></param>
        /// <param name="amount"></param>
        /// <param name="measurement"></param>
        /// <param name="category"></param>
        /// <param name="validated"></param>
        /// <returns></returns>
        private double ValidateInput(string name, string amount, string measurement, string category, out bool validated)
        {
            bool nameValidated = false;
            bool amountValidated = false;
            bool measurementValidated = false;
            bool categoryValidated = false;

            ValidateName(name, out nameValidated);
            double amt = ValidateAmount(amount, out amountValidated);
            ValidateMeasurement(measurement, out measurementValidated);
            ValidateCategory(category, out categoryValidated);

            validated = nameValidated && amountValidated && measurementValidated && categoryValidated;

            return amt;
        }

        private void ValidateName(string name, out bool validated)
        {
            validated = true;
            if (string.IsNullOrEmpty(name))
            {
                validated = false;
                NameError("");
            }
            else if (library.Contains(name))
            {
                validated = false;
                NameError("AlreadyExists");
            }
        }
        private double ValidateAmount(string amount, out bool validated)
        {
            validated = true;
            double toReturn = 0;
            if (string.IsNullOrEmpty(amount))
            {
                validated = false;
                AmountError(string.Empty);
                return toReturn;
            }

            if (amount.Contains("/"))
            {
                string[] split = amount.Split(new char[] { ' ', '/' });
                if (split.Length == 2)
                {
                    int numerator = 0;
                    int denomonator = 1;
                    if (int.TryParse(split[0], out numerator) && int.TryParse(split[1], out denomonator))
                    {
                        return denomonator == 0 ? numerator : Math.Round((double)(numerator / denomonator), 2);
                    }
                }
            }

            bool success = double.TryParse(amount, out toReturn);
            if (success && toReturn <= 0)
            {
                AmountError("<0");
                validated = false;
            }

            return toReturn;
        }
        private void ValidateMeasurement(string measurement, out bool validated)
        {
            validated = true;
            List<string> validInputs = new List<string>()
            {
                FoodItem.Metric.c.ToString(),
                FoodItem.Metric.oz.ToString(),
                FoodItem.Metric.Tbsp.ToString(),
                FoodItem.Metric.tsp.ToString(),
                FoodItem.Metric.whole.ToString()
            };

            if(string.IsNullOrEmpty(measurement) || !validInputs.Contains(measurement))
            {
                MessageBox.Show("The measurement entered is not valid, the valid measurements are:\nc, oz, Tbsp, tsp, or whole", "Invalid Input", MessageBoxButtons.OK);
                validated = false;
            }
        }
        private void ValidateCategory(string category, out bool validated)
        {
            validated = true;
            List<string> validInputs = new List<string>()
            {
                FoodItem.CategoryName.Condiment.ToString(),
                FoodItem.CategoryName.Fat.ToString(),
                FoodItem.CategoryName.Fruit.ToString(),
                FoodItem.CategoryName.Grain.ToString(),
                FoodItem.CategoryName.Protein.ToString(),
                FoodItem.CategoryName.Dressing.ToString(),
                FoodItem.CategoryName.Vegetable.ToString()
            };

            if (string.IsNullOrEmpty(category) || !validInputs.Contains(category))
            {
                MessageBox.Show("The category entered is not valid, the valid categories are:\nCondiment, Fat, Fruit, Grain, Protein, Dressing, or Vegetable", "Invalid Input", MessageBoxButtons.OK);
                validated = false;
            }
        }

        private void AmountError(string error)
        {
            string message = "";
            switch (error)
            {
                case "":
                    message = "You did not enter an amount,\ntry something like 8 or 1/2";
                    break;
                case "<0":
                    message = "The amount you entered is invalid,\ntry something like 8 or 1/2";
                    break;
            }

            MessageBox.Show(message, "Error Detected in Input", MessageBoxButtons.OK);
        }
        private void NameError(string error)
        {
            string message = "";

            switch (error)
            {
                case "":
                    message = "You did not enter a food name";
                    break;

                case "AlreadyExists":
                    message = "This food name already exists";
                    break;
            }

            string caption = "Error Detected in Input";
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);
        }

        private void AddItem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                Close();
                return true;
            }
            return base.ProcessDialogKey(keyData);
        }
        #endregion
    }
}
